const table = document.createElement("table");
document.body.appendChild(table);
table.style.borderCollapse = "collapse";

for (let i=1; i<=30; i++) {
  const row = document.createElement("tr");
  table.appendChild(row);
  row.style.height = "20px";

  for (let i=1; i<=30; i++) {
    const cell = document.createElement("td");
    row.appendChild(cell);
    cell.style.width = "20px";
    cell.style.border = "2px solid grey";
  }
}

document.body.onclick = function(event) {
  if (event.target.tagName != "TD")
    table.classList.toggle("inverted-color");
  else
    event.target.classList.toggle("selected");
};
