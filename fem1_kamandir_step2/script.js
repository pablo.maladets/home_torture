const board = document.querySelector(".board");

const bgModal = document.createElement("div");
document.body.appendChild(bgModal);
bgModal.className = "background-modal";

const visitCard = document.createElement("div");
visitCard.className = "modal-content";
bgModal.appendChild(visitCard);

// for D'n'D
let visitCards = [];

const closeBtnCreate = document.createElement("div");
closeBtnCreate.className = "close-button";
closeBtnCreate.innerText = "❌";
visitCard.appendChild(closeBtnCreate);

const boardEmptyTxt = document.querySelector(".empty-board-txt");

const htmlFragment = document.createElement("form");
htmlFragment.className = "form";
htmlFragment.innerHTML = "<label for='doctor'></label>" +
  "<select required id='doctor'>" +
  "<option class='opt-field' id='selector' value=''>-- Choose your doctor --</option>" +
  "<option class='opt-field' value='cardio'>Cardiologist</option>" +
  "<option class='opt-field' value='dent'>Dentist</option>" +
  "<option class='opt-field' value='therapy'>Therapist</option>" +
  "</select>" +
  "<label for='visitDate'>Date of planned visit</label>" +
  "<input class='inp-field' required type='date' id='visitDate'>" +
  "<input class='inp-field' required autocomplete='off' type='text' id='fullName' placeholder='Full name'>" +
  "<input class='inp-field' required type='number' id='age' placeholder='Age'>" +
  "<input class='inp-field' required type='text' id='purpose' placeholder='Purpose of the visit'>" +
  "<input class='additional inp-field' type='text' id='pressure' placeholder='Regular pressure'>" +
  "<input class='additional inp-field' type='number' id='weightI' placeholder='Weight index'>" +
  "<input class='additional inp-field' type='text' id='dHistory' placeholder='Diseases history'>" +
  "<label class='additional' for='lastVisit' id='lastVisitLabel'>Date of previous visit</label>" +
  "<input class='additional inp-field' type='date' id='lastVisit'>" +
  "<textarea class='inp-field' maxlength='400' rows='5' id='comments' placeholder='Comments'></textarea>" +
  "<button class='create-visit-btn' id='submit' disabled>Create</button>";

visitCard.appendChild(htmlFragment);

createVisitBtn.onclick = function () {
  bgModal.style.display = "block";
  visitCard.style.display = "block";
};

// declaring 2 functions to show/hide additional field(s) on certain doctor selection
// to be used for SWITCH operator
const showElements = function (...elements) {
  for (let element of elements) element.style.display = "block";
};
const hideElements = function (...elements) {
  for (let element of elements) element.style.display = null;
};

// disabling default option in select field
doctor.onclick = function () {
  selector.disabled = true;
};

// adding special input fields on selecting certain doctor with SWITCH operator
doctor.onchange = function () {
  switch (this.value) {
    case "cardio":
      showElements(pressure, weightI, dHistory);
      hideElements(lastVisit, lastVisitLabel);
      break;
    case "dent":
      showElements(lastVisit, lastVisitLabel);
      hideElements(pressure, weightI, dHistory);
      break;
    case "therapy":
      hideElements(pressure, weightI, lastVisit, dHistory, lastVisitLabel);
      break;
    default :
      hideElements(pressure, weightI, lastVisit, dHistory, lastVisitLabel);
  }
  submit.disabled = null;
};

submit.onclick = function (event) {
  event.preventDefault();
// minimalistic validation
  if (!visitDate.value || !fullName.value || !age.value || !purpose.value || (!pressure.value && pressure.style.display) || (!weightI.value && weightI.style.display) || (!dHistory.value && dHistory.style.display) || (!lastVisit.value && lastVisit.style.display)) {
    alert("Please fill in all the fields");
    return;
  }

  let visit;
  switch (doctor.value) {
    case "cardio":
      visit = new CardioVisit(visitDate.value, fullName.value, age.value, purpose.value, pressure.value, weightI.value, dHistory.value, comments.value);
      break;
    case "dent":
      visit = new DentVisit(visitDate.value, fullName.value, age.value, purpose.value, lastVisit.value, comments.value);
      break;
    case "therapy":
      visit = new TherapyVisit(visitDate.value, fullName.value, age.value, purpose.value, comments.value);
      break;
  }
  visit.shortCard();
  visits.push(visit);

  localStorage.visits = JSON.stringify(visits);
};

// closing modal window function on pressing "x" button
const CreateNoteCloseBtn = document.querySelector(".close-button");
CreateNoteCloseBtn.onclick = function () {
  visitCard.style.display = null;
  bgModal.style.display = "none";
};

// closing modal window function on blurring modal window
bgModal.onclick = function (event) {
  if (event.target === this) {
    visitCard.style.display = null;
    bgModal.style.display = "none";
  }
};

// creating common class VISIT
class Visit {
  constructor(visitDate, fullName, doctor, age, purpose, comments) {
    this.visitDate = visitDate;
    this.patientName = fullName;
    this.doctor = doctor;
    this.age = age;
    this.purpose = purpose;
    this.comments = comments;
  };

  // appending the minimalistic card to board
  shortCard() {
    const note = document.createElement("div");
    note.className = "note";
    note.innerHTML = "<span class=\"note-del-btn\">&times;</span>" +
      "<p class='note-visit-date'>" + this.visitDate + "</p>" +
      "<p class='note-full-name'>" + this.patientName + "</p>" +
      "<p class='note-doctor'>" + this.doctor + "</p>" +
      "<span class='note-btn note-show-all'>more...</span>";
    board.appendChild(note);
    visitCard.style.display = null;
    bgModal.style.display = "none";
    boardEmptyTxt.style.display = "none";

    // special feature to prevent standard browser drop
    // copied from not properly working example on javascript.info;
    note.ondragstart = () => false;

    const self = this;
    const closeNoteBtn = note.querySelector(".note-del-btn");

    closeNoteBtn.onclick = function () {
      note.remove();
      visits = visits.filter(visit => visit !== self);
      localStorage.visits = JSON.stringify(visits);
      boardEmptyTxt.style.display = visits.length ? "none" : "block";
    };
    const details = note.querySelector(".note-show-all");

    details.onclick = moreLess.bind(this);

    visitCards.push(note);
    visitCards.forEach((card, i) => card.style.zIndex = i);
  }

  // inserting common additional fields and changing button name
  fullCard(event) {
    const fullCardBlock = document.createElement("div");
    fullCardBlock.className = "note-full-card-block";
    const ageLine = document.createElement("p");
    ageLine.textContent = "Age: " + this.age;
    const purposeLine = document.createElement("p");
    purposeLine.textContent = "Purpose: " + this.purpose;
    const comments = document.createElement("p");
    comments.textContent = "Comments: " + this.comments;
    const note = event.target.parentElement;
    note.appendChild(fullCardBlock);
    fullCardBlock.append(document.createElement("hr"), ageLine, purposeLine, comments);
    return fullCardBlock;
  }
}

const moreLess = function (event) {
  if (event.target.textContent !== "less...") {
    event.target.textContent = "less...";
    this.fullCard(event);
  } else {
    event.target.textContent = "more...";
    event.target.parentElement.querySelector(".note-full-card-block").remove();
  }
};

Visit.shortCard = visit => Visit.prototype.shortCard.call(visit);

class CardioVisit extends Visit {
  constructor(visitDate, fullName, age, purpose, pressure, weightI, dHistory, comments) {
    super(visitDate, fullName, "Cardiologist", age, purpose, comments);
    this.pressure = pressure;
    this.weightI = weightI;
    this.dHistory = dHistory;
  };

  fullCard(event) {
    const fullCardBlock = super.fullCard(event);
    const pressureLine = document.createElement("p");
    pressureLine.textContent = "Pressure: " + this.pressure;
    const weightLine = document.createElement("p");
    weightLine.textContent = "Weight Index: " + this.weightI;
    const historyLine = document.createElement("p");
    historyLine.textContent = "Decease history: " + this.dHistory;
    fullCardBlock.append(pressureLine, weightLine, historyLine, [...fullCardBlock.children].pop());
  }
}

class DentVisit extends Visit {
  constructor(visitDate, fullName, age, purpose, lastVisit, comments) {
    super(visitDate, fullName, "Dentist", age, purpose, comments);
    this.lastVisit = lastVisit;
  };

  fullCard(event) {
    const fullCardBlock = super.fullCard(event);
    const lastVisitLine = document.createElement("p");
    lastVisitLine.textContent = "Last visit: " + this.lastVisit;
    [...fullCardBlock.children].pop().insertAdjacentElement("beforebegin", lastVisitLine);
  };
}

class TherapyVisit extends Visit {
  constructor(visitDate, fullName, age, purpose, comments) {
    super(visitDate, fullName, "Therapist", age, purpose, comments);
  };
}

let visits = JSON.parse(localStorage.visits || "[]");
visits.forEach(visit => {
  const visitProto = [CardioVisit, DentVisit, TherapyVisit][["Cardiologist", "Dentist", "Therapist"].indexOf(visit.doctor)].prototype;
  Object.setPrototypeOf(visit, visitProto);
  Visit.shortCard(visit)
});

boardEmptyTxt.style.display = visits.length ? "none" : "block";

// ****************   D R A G  and  D R O P   ****************

board.onmousedown = function (event) {

  if (event.target === board) return;

  const note = event.path.find(element => element.classList.contains("note"));

  // for D'n'D - selected card to be above all other cards
  visitCards = [...visitCards.filter(card => card !== note), note];
  visitCards.forEach((card, i) => card.style.zIndex = i);

  const {x: noteX, y: noteY, width, height} = note.getBoundingClientRect();
  const offsetX = event.clientX - noteX;

  const offsetY = event.clientY - noteY;

  const {x: leftEdge, y: topEdge, bottom: bottomEdge, right: rightEdge} =
    board.getBoundingClientRect();

  const teleport = function(x, y) {
    note.style.position = "absolute";
    note.style.left = Math.min(Math.max(x, leftEdge), rightEdge - width - 10) + "px";
    note.style.top = Math.min(Math.max(y, topEdge), bottomEdge - height - 10) + "px";
  };

  document.body.onmousemove = function (event) {
    teleport(event.clientX - offsetX, event.clientY - offsetY);
  };

  document.body.onmouseup = function (event) {
    document.body.onmousemove = null;
  };
};