/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

try {
  function Hamburger(size, stuffing) {
    if (Object.values(Hamburger).indexOf(size) < 0 || !size.size) {
      throw new HamburgerException("size");
    }
    if (Object.values(Hamburger).indexOf(stuffing) < 0 || !stuffing.stuffing) {
      throw new HamburgerException("stuffing");
    }
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
  }
} catch (error) {
  if (error instanceof HamburgerException) console.error(error.message);
  else throw error;
}

Hamburger.SIZE_SMALL = {size: "small", price: 50, cal: 20};
Hamburger.SIZE_LARGE = {size: "large", price: 100, cal: 40};
Hamburger.STUFFING_CHEESE = {stuffing: "cheese", price: 10, cal: 20};
Hamburger.STUFFING_SALAD = {stuffing: "salad", price: 20, cal: 5};
Hamburger.STUFFING_POTATO = {stuffing: "potato", price: 15, cal: 10};
Hamburger.TOPPING_MAYO = {topping: "mayonnaise", price: 20, cal: 5};
Hamburger.TOPPING_SPICE = {topping: "spice", price: 15, cal: 0};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
  if (this.toppings.indexOf(topping) < 0) {
    this.toppings.push(topping);
  }
  else {
    throw new HamburgerException("repeat");
  }
};

 /**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
  if (this.toppings.indexOf(topping) < 0) {
    throw new HamburgerException("absent");
  }
  this.toppings.splice(this.toppings.indexOf(topping), 1);
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
  return this.toppings;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
  return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
  return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
  return this.size.price + this.stuffing.price
    + this.toppings.reduce(function(sum, topping) {
      return sum+topping.price;
    }, 0)

};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
  return this.size.cal + this.stuffing.cal
    + this.toppings.reduce(function(sum, topping) {
      return sum+topping.cal;
    }, 0);
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException (errorName) {
  switch (errorName) {
    case "absent":
      this.message = "No such topping on this hamburger.";
      break;
    case "repeat" :
      this.message = "This topping is already added";
      break;
    case "size":
      this.message = "Incorrect size";
      break;
    case "stuffing":
      this.message = "Incorrect stuffing";
      break;
  }
}


/*
try {
  var hamb = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
} catch (error) {
  if (error instanceof HamburgerException) console.error(error.message);
  else throw error;
}
console.log(hamb);
*/

var hamb = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamb);