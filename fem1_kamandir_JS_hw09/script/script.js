﻿const content = document.getElementsByClassName("tab-content");
for (let x=1; x<content.length; x++) {
    content[x].style.display = "none";
}
const tab = document.getElementById("lol");
for (let y=0; y<tab.children.length; y++) {
    tab.children[y].dataset.index = `${y}`;
}
tab.onclick = (klats) => {
    tab.querySelector(".active").classList.remove("active");
    klats.target.classList.add("active");
    for (let z=0; z<content.length; z++) {
        content[z].style.display = "none";
    }
    content[klats.target.dataset.index].style.display = "block";
};
