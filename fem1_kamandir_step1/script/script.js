// ******************** S E R V I C E   T A B S ********************

const serviceTabs = document.getElementsByClassName("service-tab");

const serviceContent = document.getElementsByClassName("service-content");

for (let i = 0; i < serviceTabs.length; i++) {
  serviceTabs[i].dataset.index = `${i}`;
}

const serviceContentSelect = function () {
  for (let tab of serviceTabs)
    tab.classList.remove("active");
  this.classList.add("active");
  for (let content of serviceContent)
    content.classList.remove("active");
  serviceContent[this.dataset.index].classList.add("active");
};
for (let tab of serviceTabs) tab.onclick = serviceContentSelect;

// ******************** C A R O U S E L ********************

const smallAvatars = document.getElementsByClassName("small-avatar");
const reviews = document.getElementsByClassName("persons-review-block");

const quoteSelect = function () {
  for (let avatar of smallAvatars)
    avatar.classList.remove("active");
  this.classList.add("active");
  for (let review of reviews)
    review.classList.remove("active");
  reviews[this.dataset.num].classList.add("active");
};

for (let avatar of smallAvatars)
  avatar.onclick = quoteSelect;

const [carouselLeft, carouselRight] = document.querySelectorAll(".carousel-butt");

carouselLeft.onclick = function () {
  const num = +document.querySelector(".small-avatar.active").dataset.num;
  smallAvatars[num ? num - 1 : 3].onclick();
};

carouselRight.onclick = function () {
  const num = +document.querySelector(".small-avatar.active").dataset.num;
  smallAvatars[num === 3 ? 0 : num + 1].onclick();
};


// ******************** I M A G E   G A L L E R Y ********************

const cat = document.querySelector(".work");
const catList = document.querySelectorAll(".work-category");
const infoCards = document.querySelectorAll(".example-item");

cat.onclick = function (event) {
  for (let tab of catList) {
    tab.classList.remove("active");
  }
  event.target.classList.add("active");

  const catName = event.target.dataset.cat;
  for (let card of infoCards) {
    if (!catName || card.classList.contains(catName)) {
      card.style.display = null;
    } else {
      card.style.display = "none";
    }
  }
};

// ******************** Load More Button  ********************

// ********* 1st attempt *********

// const imageGallery = document.querySelector(".work-examples-block");
// const imageLoader = function () {
//     let counter = 0;
//     for (let div of imageGallery.children) {
//         if (counter === 12) break;
//         if (div.classList.contains("hidden")) {
//             div.classList.remove("hidden");
//             counter++;
//         }
//     }
// };

// ********* 2nd attempt *********

const imageLoader = function () {
  let next12 = [...document.querySelectorAll(".work-examples-block .hidden")]
    .slice(0, 12);
  next12.forEach(div => div.classList.remove("hidden"));
};

loadMoreButt.onclick = imageLoader;

// ******************** white card *********************/

const hideImg = function () {
  this.children[0].style.display = "none";
  curtain.style.display = "block";

// const cats = [...cat.children]
//   .map(tab => tab.dataset.cat);
// let catName = cats.find(cat => this.classList.contains(cat));
// catText.innerText = [...catList].find(tab => tab.dataset.cat === catName).innerText;

  catText.innerText = [...cat.children].find(tab => this.classList.contains(tab.dataset.cat)).innerText;

  this.appendChild(curtain);
};

const showImg = function () {
  this.children[0].style.display = null;
  curtain.style.display = null;
};

for (let card of infoCards) {
  card.onmouseenter = hideImg;
  card.onmouseleave = showImg;
}

// *********** "A W E S O M E"   M A S O N R Y     P L U G I N   at   work ************/

var elem = document.querySelector('.gallery-container');
var msnry = new Masonry(elem, {
// options
  itemSelector: '.grid-item',
  columnWidth: 370
// element argument can be a selector string
//   for an individual element
});

var msnry = new Masonry('.gallery-container', {
  gutter: 10,
  horizontalOrder: true,
  fitWidth: true
// options
});
