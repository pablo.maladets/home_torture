const pwdFirst = document.getElementById("password-1st");
const pwdSecond = document.getElementById("password-2nd");
const confirm = document.getElementById("check");

const inputs = document.querySelectorAll(".input-field");
// console.log(inputs);
const warnMsgRem = function () {
  inputs.forEach(function (element) {
    element.onfocus = function () {
      warning.style.display = "none"
    };
  });
};

confirm.onclick = (event) => {
  if (pwdFirst.value === pwdSecond.value) {
    alert("You are welcome!");
    // pwdSecond.onfocus = function () {
    //   warning.style.display = "none";
    // };
    warnMsgRem();
  } else {
    warning.style.display = null;
  }
  event.preventDefault();
};

warnMsgRem();
// pwdSecond.onfocus = function () {
//   warning.style.display = "none";
// };

const eyes = document.querySelectorAll(".icon-password");

eyes[0].onclick = eyes[1].onclick = function () {
  this.classList.toggle("fa-eye");
  this.classList.toggle("fa-eye-slash");
  const input = this.parentNode.querySelector("input");
  input.type = (input.type === "text") ? "password" : "text";
};