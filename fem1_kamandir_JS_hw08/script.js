const input = document.getElementsByClassName("price-input")[0];

input.addEventListener("focus",function() {
    this.classList.add("shadow");
    this.style.outline = "none";
});
input.addEventListener("blur",function() {
    if (input.value < 0) {
        warning.style.visibility = "visible";
    } else {
        this.classList.remove("shadow");
        const value = this.value;
        currentPrice.textContent =
            currentPrice.textContent.replace(/\$.*/,"$" + value);
        [currentPrice,closeButt].forEach(el =>
            el.style.visibility = "visible");
    }
});

closeButt.onclick = function () {
    [currentPrice,closeButt].forEach(el  => el.style.visibility = "hidden");
    input.value = ""
}
