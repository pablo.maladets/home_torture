﻿let imgIndex = 1;
let imgNum = document.querySelectorAll("img.image-to-show");
imgNum[0].style.opacity = "1";
let timerID = setInterval(function(){
    imgNum.forEach(function(item, index) {
        if(index !== imgIndex % imgNum.length) {
            item.style.opacity = "0";
        } else {
            item.style.opacity = "1";
        }
    });
    imgIndex++
}, 1000);

const resBtn = document.createElement("button");
document.body.appendChild(resBtn);
resBtn.textContent = "Погнали";
resBtn.style.cssFloat = "right";

const stopBtn = document.createElement("button");
document.body.appendChild(stopBtn);
stopBtn.textContent = "Тормози";
stopBtn.style.cssFloat = "right";

stopBtn.onclick = function() {
    clearInterval(timerId);
